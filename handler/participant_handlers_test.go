package handler

import (
	"net/http"
	"reflect"
	"testing"

	"github.com/100sHoldings/100s-connamaratech-adapter/internal/app/service"
)

func TestCreateParticipantHandler(t *testing.T) {
	type args struct {
		s *service.Participant
	}
	tests := []struct {
		name string
		args args
		want http.HandlerFunc
	}{
		{
			name: "",
			args: args{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CreateParticipantHandler(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateParticipantHandler() = %v, want %v", got, tt.want)
			}
		})
	}
}
