package handler

import (
	"encoding/json"
	"net/http"

	"github.com/100sHoldings/100s-connamaratech-adapter/internal/app/model"
	"github.com/100sHoldings/100s-connamaratech-adapter/internal/app/service"
	"google.golang.org/grpc/metadata"
)

func CreateParticipantHandler(s *service.Participant) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var participantData model.Participant
		if err := json.NewDecoder(r.Body).Decode(&participantData); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		md := metadata.New(map[string]string{"Authorization": r.Header.Get("Authorization")})
		ctx := metadata.NewOutgoingContext(r.Context(), md)

		participant, err := s.CreateParticipant(ctx, participantData)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(participant)
	}
}
