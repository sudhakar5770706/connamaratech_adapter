# 1. Golang Architecture Decisions

Date: 2024-04-20

## Status

Proposed


## Context

We need to record the architectural decisions made on this project. 

#### Proposed By 
Sudhakar Annadurai, Inder Singh, Parth Patoliya

#### Proposed Idea 1 

![proposal_1.png](images%2Fproposal_1.png)


#### Explanation
for the user creation flow we will be calling golang service from Nest API through REST API, for Event based bets Front End (Next App) will directly call Golang service through websocket


Proposed Idea 2
![proposal_2.png](images%2Fproposal_2.png)

Explanation

Here in the above diagram frontend(Next Service) is calling User logic Backend (Nest Service) we can persist the data and return to front end immediately 
Internally Golang Service (Connamara Wrapper) will consume the data from queue and process and update it in DB




## Decision

We are going to use Proposed Idea #1

## Consequences

Write if any