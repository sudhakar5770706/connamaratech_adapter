package service

import (
	"context"
	"github.com/100sHoldings/100s-connamaratech-adapter/config"
	"github.com/100sHoldings/100s-connamaratech-adapter/internal/app/model"

	pb "github.com/100sHoldings/100s-connamaratech-adapter/external/generated/connamara/ep3/admin/v1beta1"
	"google.golang.org/grpc"
)

type Participant struct {
	connamaraAdmin pb.AdminAPIClient
	config         config.Configuration
}

func NewParticipant(conn *grpc.ClientConn, config config.Configuration) *Participant {
	return &Participant{
		connamaraAdmin: pb.NewAdminAPIClient(conn),
		config:         config,
	}
}

func (s *Participant) CreateParticipant(ctx context.Context, participantData model.Participant) (model.Participant, error) {
	participant := &pb.Participant{
		Name:        participantData.UserName,
		Firm:        s.config.Participant.DefaultFirm,
		DisplayName: participantData.LegalName,
		Role:        pb.ParticipantRole_PARTICIPANT_ROLE_TRADING,
		State:       pb.ParticipantState_PARTICIPANT_STATE_ACTIVE,
	}

	request := &pb.CreateParticipantRequest{
		Participant: participant,
		Parent:      s.config.Participant.DefaultFirm,
	}

	_, err := s.connamaraAdmin.CreateParticipant(ctx, request)
	if err != nil {
		return model.Participant{}, err
	}

	return participantData, nil
}
