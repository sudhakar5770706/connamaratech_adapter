package service

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"github.com/100sHoldings/100s-connamaratech-adapter/config"
	pb1 "github.com/100sHoldings/100s-connamaratech-adapter/external/generated/connamara/ep3/admin/v1beta1"
	pb "github.com/100sHoldings/100s-connamaratech-adapter/external/generated/connamara/ep3/admin/v1beta1/mock"
	"github.com/100sHoldings/100s-connamaratech-adapter/internal/app/model"
	"go.uber.org/mock/gomock"
	"google.golang.org/grpc"
)

func TestParticipant_CreateParticipant(t *testing.T) {
	type args struct {
		ctx         context.Context
		participant model.Participant
		firm        string
	}
	var tests = []struct {
		name     string
		args     args
		want     model.Participant
		throwErr bool
		wantErr  bool
	}{
		{
			name: "should return error response if client gives error response",
			args: args{
				ctx:         context.Background(),
				participant: model.Participant{},
			},
			want:     model.Participant{},
			throwErr: true,
			wantErr:  true,
		},
		{
			name: "should return participant response if client gives proper response",
			args: args{
				ctx: context.Background(),
				participant: model.Participant{
					UserName:    "test",
					UserID:      "user001",
					LegalName:   "test",
					Email:       "test@test.com",
					PhoneNumber: "123456789",
					DOB:         "12/04/1994",
					Address:     "chennai",
				},
			},
			want: model.Participant{
				UserName:    "test",
				UserID:      "user001",
				LegalName:   "test",
				Email:       "test@test.com",
				PhoneNumber: "123456789",
				DOB:         "12/04/1994",
				Address:     "chennai",
			},
			throwErr: false,
			wantErr:  false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			connamaraAdminMock := pb.NewMockAdminAPIClient(ctrl)
			if tt.throwErr {
				connamaraAdminMock.EXPECT().CreateParticipant(gomock.Any(), gomock.Any()).Return(&pb1.CreateParticipantResponse{}, errors.New("something went wrong"))
			} else {
				connamaraAdminMock.EXPECT().CreateParticipant(gomock.Any(), gomock.Any()).Return(&pb1.CreateParticipantResponse{}, nil)
			}

			s := &Participant{
				connamaraAdmin: connamaraAdminMock,
				config: config.Configuration{
					Participant: config.Participant{
						DefaultFirm: "firm",
					},
				},
			}
			got, err := s.CreateParticipant(tt.args.ctx, tt.args.participant)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateParticipant() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateParticipant() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewParticipant(t *testing.T) {

	type args struct {
		conn   *grpc.ClientConn
		config config.Configuration
	}
	tests := []struct {
		name string
		args args
		want *Participant
	}{
		{
			name: "Should return participant when New Participant called with proper data",
			args: args{
				conn:   &grpc.ClientConn{},
				config: config.Configuration{},
			},
			want: &Participant{
				connamaraAdmin: pb1.NewAdminAPIClient(&grpc.ClientConn{}),
			},
		},
	}
	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {
			if got := NewParticipant(tt.args.conn, tt.args.config); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewParticipant() = %v, want %v", got, tt.want)
			}
		})
	}
}
