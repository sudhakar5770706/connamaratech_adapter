package model

type Participant struct {
	UserName    string `json:"userName"`
	UserID      string `json:"userID"`
	LegalName   string `json:"legalName"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phoneNumber"`
	DOB         string `json:"DOB"`
	Address     string `json:"address"`
}
