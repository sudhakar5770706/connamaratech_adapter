package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"

	"github.com/100sHoldings/100s-connamaratech-adapter/config"
	"github.com/100sHoldings/100s-connamaratech-adapter/handler"
	"github.com/100sHoldings/100s-connamaratech-adapter/internal/app/service"
	"github.com/gorilla/mux"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func main() {

	config, err := config.Load()

	conn, err := grpc.Dial(config.ConnamaraEndpoint, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{})))

	if err != nil {
		log.Fatalf("Failed to connect: %v", err)
	}
	defer conn.Close()

	participantService := service.NewParticipant(conn, config)

	router := mux.NewRouter()

	router.HandleFunc("/participants", handler.CreateParticipantHandler(participantService)).Methods("POST")

	fmt.Println("running server :", config.Port)
	log.Fatal(http.ListenAndServe(":"+config.Port, router))
}
