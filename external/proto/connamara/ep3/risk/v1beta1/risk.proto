// Copyright (c) 2022 Connamara Technologies, Inc.
//
// This file is distributed under the terms of the Connamara EP3 Software License Agreement.
//
// The above copyright notice and license notice shall be included in all copies or substantial portions of the Software.

syntax = "proto3";
package connamara.ep3.risk.v1beta1;

option go_package = "./riskv1beta1";
option java_package = "com.connamara.ep3.risk.v1beta1";
option java_outer_classname = "RiskProto";
option java_multiple_files = true;
option csharp_namespace = "Connamara.Ep3.Risk.V1Beta1";
option objc_class_prefix = "CER";

import "google/protobuf/timestamp.proto";
import "connamara/ep3/firms/v1beta1/firms.proto";
import "connamara/ep3/type/v1beta1/type.proto";

// SecurityType is a code to identify the classification of the security.
enum SecurityType {
    // Undefined state of the security type
    SECURITY_TYPE_UNDEFINED = 0;

    // Unique identifier that stands for the Committee on Uniform Securities Identification Procedures
    SECURITY_TYPE_CUSIP = 1;

    // Stands for the Stock Exchange Daily Official List assigned to securities that trade on LSEG
    SECURITY_TYPE_SEDOL = 2;

    // Japanese domestic code
    SECURITY_TYPE_QUIK = 3;

    // International Securities Identification Number
    SECURITY_TYPE_ISIN = 4;

    // ISO Standard currency code
    SECURITY_TYPE_ISO_CURRENCY = 5;

    // Reuters Instrument Code
    SECURITY_TYPE_RIC_CODE = 6;
}

// SecurityDefinition contains descriptive meta data regarding a security.
message SecurityDefinition {
    //Security ID, which may be a CUSIP, market symbol, or similar unique identifier
    string security_id = 1;

    //Plaintext description of the security
    string description = 2;

    //The type of the security
    SecurityType security_type = 3;
}

// WithdrawalType defines the type of withdrawal.
enum WithdrawalType {
    // Undefined state of the withdrawal type
    WITHDRAWAL_TYPE_UNDEFINED = 0;

    // Specifies the withdrawal is for a currency
    WITHDRAWAL_TYPE_CURRENCY = 1;

    // Specifies the withdrawal is for a security
    WITHDRAWAL_TYPE_SECURITY = 2;
}

// PendingWithdrawal describes a pending withdrawal of fiat currency from a given account.
message PendingWithdrawal {
    //Unique identifier for the withdrawal.
    string id = 1;

    //Account name.
    string name = 2;

    //Fiat currency.
    string currency = 3;

    //Balance to withdraw.
    string balance = 4;

    //Description for the withdrawal request.
    string description = 5;

    //Bank details of the recipient.
    firms.v1beta1.BankDetails bank_recipient = 6;

    //If set to true, indicates that the pending withdrawal has been acknowledged by a downstream process and should not be deleted. 
    bool acknowledged = 7;

    //Security ID.
    string security_id = 8;

    // The time at which this pending withdrawal was created.
    google.protobuf.Timestamp creation_time = 9;
}

// PendingCredit describes a pending credit of fiat currency to a given account.
message PendingCredit {
    //Unique identifier for the credit.
    string id = 1;

    //Account name.
    string name = 2;

    //Fiat currency.
    string currency = 3;

    //If the pending credit is approved, the account's fiat balance will be increased by this amount.
    string balance = 4;

    //If the pending credit is approved, this is the description that EP3 will include on the Balance Ledger entry that it creates due to the change in the account's fiat balance.
    string description = 5;

    //An optional identifier of the pending credit to filter upon that also supports regexes.
    string correlation_id = 6;

    //If the pending credit is approved, this is the ledger entry type that EP3 will include on the Balance Ledger entry that it creates due to the change in the account's fiat balance.
    type.v1beta1.LedgerEntryType entry_type = 7;

    // The time at which this pending credit was created.
    google.protobuf.Timestamp creation_time = 8;
}

// PendingSettlement describes a pending settlement of fiat currency or position to a given account.
message PendingSettlement {
    //Unique identifier for the trade that is pending settlement.
    string trade_id = 1;

    //Account name.
    string name = 2;

    //Fiat currency.
    string currency = 3;

    //If the pending settlement is cleared, the account's fiat balance will be increased by this amount.
    string balance = 4;

    //If the pending settlement is cleared, the account's position will be increased for this symbol.
    string symbol = 5;

    //If the pending settlement is cleared, the account's position will be increased for this sub type.
    string symbol_sub_type = 6;

    //If the pending settlement is cleared, the account's position will be increased by this amount. Note that this value is in fixed point integer representation and must be scaled per the instrument.
    int64 position = 7;
}
