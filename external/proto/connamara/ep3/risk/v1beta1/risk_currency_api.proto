// Copyright (c) 2022 Connamara Technologies, Inc.
//
// This file is distributed under the terms of the Connamara EP3 Software License Agreement.
//
// The above copyright notice and license notice shall be included in all copies or substantial portions of the Software.

syntax = "proto3";
package connamara.ep3.risk.v1beta1;

option go_package = "./riskv1beta1";
option java_package = "com.connamara.ep3.risk.v1beta1";
option java_outer_classname = "RiskCurrencyApiProto";
option java_multiple_files = true;
option csharp_namespace = "Connamara.Ep3.Risk.V1Beta1";
option objc_class_prefix = "CER";

import "connamara/ep3/risk/v1beta1/risk.proto";
import "connamara/ep3/firms/v1beta1/firms.proto";
import "connamara/ep3/type/v1beta1/type.proto";
import "google/protobuf/timestamp.proto";

//RiskCurrencyAPI is the API for managing currency balances for risk engines.
service RiskCurrencyAPI {
    //GetAccountBalance gets the balance for currency in an account.
    rpc GetAccountBalance(GetAccountBalanceRequest) returns (GetAccountBalanceResponse) {}

    //GetAccountBalanceLedger gets the balance ledger for a currency in an account.
    rpc GetAccountBalanceLedger(GetAccountBalanceLedgerRequest) returns (GetAccountBalanceLedgerResponse) {}

    //StreamAccountBalanceLedger streams the balance ledger for a currency in an account.
    rpc StreamAccountBalanceLedger(StreamAccountBalanceLedgerRequest) returns (stream StreamAccountBalanceLedgerResponse) {}

    //ListAccountBalances lists the currency balances in an account.
    rpc ListAccountBalances(ListAccountBalancesRequest) returns (ListAccountBalancesResponse) {}

    //AdjustAccountBalance updates the balance for currency in an account.
    rpc AdjustAccountBalance(AdjustAccountBalanceRequest) returns (AdjustAccountBalanceResponse) {}

    //SetAccountBalance sets the balance for currency in an account.
    rpc SetAccountBalance(SetAccountBalanceRequest) returns (SetAccountBalanceResponse) {}

    //SetAccountCapitalRequirement sets the capital requirement for a currency in an account.
    rpc SetAccountCapitalRequirement(SetAccountCapitalRequirementRequest) returns (SetAccountCapitalRequirementResponse) {}

    //SetAccountSecurityBalance sets the security balance to back the collateral for a base currency in an account.
    rpc SetAccountSecurityBalance(SetAccountSecurityBalanceRequest) returns (SetAccountSecurityBalanceResponse) {}

    //SetAccountSecurityMarketValue sets the security market value to back the collateral for a base currency in an account.
    rpc SetAccountSecurityMarketValue(SetAccountSecurityMarketValueRequest) returns (SetAccountSecurityMarketValueResponse) {}

    //ListSecurityDefinitions fetches security descriptions and types for each known identifier on the system.
    rpc ListSecurityDefinitions(ListSecurityDefinitionsRequest) returns (ListSecurityDefinitionsResponse) {}

    //SetSecurityDefinition assigns a description and type to a known identifier on the system for later reference.
    rpc SetSecurityDefinition(SetSecurityDefinitionRequest) returns (SetSecurityDefinitionResponse) {}

    //CreatePendingWithdrawal creates a request to withdraw currency from an account.
    rpc CreatePendingWithdrawal(CreatePendingWithdrawalRequest) returns (CreatePendingWithdrawalResponse) {}

    //ListPendingWithdrawals lists the pending withdrawals of an account.
    rpc ListPendingWithdrawals(ListPendingWithdrawalsRequest) returns (ListPendingWithdrawalsResponse) {}

    //GetPendingWithdrawal gets a pending withdrawal by unique identifier.
    rpc GetPendingWithdrawal(GetPendingWithdrawalRequest) returns (GetPendingWithdrawalResponse) {}

    //DeletePendingWithdrawal deletes a pending withdrawal of an account.
    rpc DeletePendingWithdrawal(DeletePendingWithdrawalRequest) returns (DeletePendingWithdrawalResponse) {}

    //AcknowledgePendingWithdrawal acknowledges a pending withdrawal of an account.
    rpc AcknowledgePendingWithdrawal(AcknowledgePendingWithdrawalRequest) returns (AcknowledgePendingWithdrawalResponse) {}

    //ApprovePendingWithdrawal approves a pending withdrawal of an account and applies the balance adjustment.
    rpc ApprovePendingWithdrawal(ApprovePendingWithdrawalRequest) returns (ApprovePendingWithdrawalResponse) {}

    //CreatePendingCredit creates a request to credit currency to an account.
    rpc CreatePendingCredit(CreatePendingCreditRequest) returns (CreatePendingCreditResponse) {}

    //ListPendingCredits lists the pending credits of an account.
    rpc ListPendingCredits(ListPendingCreditsRequest) returns (ListPendingCreditsResponse) {}

    //GetPendingCredit gets a pending credit by unique identifier.
    rpc GetPendingCredit(GetPendingCreditRequest) returns (GetPendingCreditResponse) {}

    //DeletePendingCredits deletes all pending credits matching the given filter.
    rpc DeletePendingCredits(DeletePendingCreditsRequest) returns (DeletePendingCreditsResponse) {}

    //ApprovePendingCredits approves all pending credits matching the given filter and applies the balance adjustments.
    rpc ApprovePendingCredits(ApprovePendingCreditsRequest) returns (ApprovePendingCreditsResponse) {}

    //ListPendingSettlement lists the trades pending risk settlement within an account.
    rpc ListPendingSettlement(ListPendingSettlementRequest) returns (ListPendingSettlementResponse) {}
}

message GetAccountBalanceRequest {
    //Account name
    string name = 1;

    //Fiat currency
    string currency = 2;
}

message GetAccountBalanceResponse {
    //Fixed point decimal fiat currency balance, not including any security values
    string balance = 1;

    //Required amount of fiat currency balance that must be held in an account and cannot be used for trading; calculated at time of request if an alternate currency is used
    string capital_requirement = 2;

    //Amount of excess capital in the balance beyond the requirement, factoring in all security valuations
    string excess_capital = 3;

    //Amount of unencumbered capital available for trading, factoring in all security valuations as well as any open orders. Set to empty string in the case of historical requests on the account ledger, otherwise represents the instantaneous buying power
    string buying_power = 4;

    //SecurityEntry describes the detailed valuation of a security with respect to the market price and adjustments applied to augment a base currency's collateral.
    message SecurityEntry {
        // Amount of the security held in the account
        string balance = 1;

        // The actual mark to market value of the security
        string market_value = 2;

        // The haircut value of a single unit of the security ID in units of basis points (100 basis points is 1%)
        int64 haircut = 3;

        // Represents the notional value of the security, which is the balance multiplied by the market value
        string notional_value = 4;

        // Represents the available collateral value of the security, which is the balance multiplied by the market value less the haircut percentage
        string available_value = 5;
    }

    //Securities mapped by ID to their detailed valuation with respect to the market price and adjustments applied
    map<string, SecurityEntry> securities = 5;
    
    //Aggregate notional value of all securities
    string total_security_notional_value = 6;
    
    //Aggregate available collateral value of all securities
    string total_security_available_value = 7;

    //PendingCredits is a list of all active Pending Credits for a given account and currency pair
    repeated PendingCredit pending_credits = 8;

    //Aggregate value of all pending credits.
    string total_pending_credit_value = 9;

    //Aggregate total notional value of open orders. Set to empty string in the case of historical requests on the account ledger.
    string open_orders = 10;

    // If exists, the alternate currency used for capital requirement.
    string alternate_capital_requirement_currency = 11;

    // The fixed amount of capital requirement in the alternate capital requirement currency.
    string alternate_capital_requirement = 12;

    //Aggregate total notional value of unsettled funds not yet available to trade. Set to empty string in the case of historical requests on the account ledger.
    string unsettled_funds = 13;
}

message ListAccountBalancesRequest {
    //Account name
    string name = 1;
}

message ListAccountBalancesResponse {
    //Balances mapped by currency key
    map<string, GetAccountBalanceResponse> balances = 1;
}

message GetAccountBalanceLedgerRequest {
    //Account name
    string name = 1;

    //Fiat currency
    string currency = 2;

    //Description searches the eponymous field for the reason behind a change in the ledger, and supports Regexes
    string description = 3;

    //Start of the time range
    google.protobuf.Timestamp start_time = 4;

    //End of the time range
    google.protobuf.Timestamp end_time = 5;

    //Filter to a specific transaction type
    repeated type.v1beta1.LedgerEntryType entry_types = 6;

    //The maximum number of objects to return in a response.
    //This field is optional.
    int32 page_size = 7;

    //A pagination token returned from a previous call
    //that indicates where this listing should continue from.
    //This field is optional.
    string page_token = 8;

    //Order by earliest start time first.
    bool newest_first = 9;
}

message GetAccountBalanceLedgerResponse {
    //BalanceChange holds a single change to account balance.
    message BalanceChange {
        //Balance before change
        GetAccountBalanceResponse before_balance = 1;

        //Balance after change
        GetAccountBalanceResponse after_balance = 2;

        //Description of why the balance change was made
        string description = 3;

        //Time at which the balance change was made
        google.protobuf.Timestamp update_time = 4;

        //ID of the security that was changed, if security was not changed, set to empty string
        string modified_security_id = 5;

        //Type of the transaction
        type.v1beta1.LedgerEntryType entry_type = 6;
    }

    //Ledger of all balance changes for a specific currency/account pair
    repeated BalanceChange ledger = 1;

    // A pagination token returned from a previous call to `GetAccountBalanceLedger`
    // This field is optional.
    string next_page_token = 2;

    //Eof is true if no more entries given the filtering criteria are available at this time.
    bool eof = 3;
}

message StreamAccountBalanceLedgerRequest {
    // The filter request to begin the stream.
    GetAccountBalanceLedgerRequest request = 1;

    // If true, keep the connection open for incremental updates.
    bool stay_open = 2;
}

message StreamAccountBalanceLedgerResponse {
    // A ledger entry returned by the given query filter. If not populated, this message is a heartbeat.
    GetAccountBalanceLedgerResponse response = 1;
}

message AdjustAccountBalanceRequest {
    //Account name
    string name = 1;

    //Fiat currency
    string currency = 2;

    //Adjustment amount
    string delta = 3;

    //Description of why the balance adjustment was made
    string description = 4;

    //Optional account name for the source of these funds, indicating a transfer of assets from one account to the next
    string source_account_name = 5;

    //Entry type to display in the balance ledger
    type.v1beta1.LedgerEntryType entry_type = 6;
}

message AdjustAccountBalanceResponse {
}

message SetAccountBalanceRequest {
    //Account name
    string name = 1;

    //Fiat currency
    string currency = 2;

    //Balance to set
    string balance = 3;

    //Description for why the balance was set
    string description = 4;

    //Entry type to display in the balance ledger
    type.v1beta1.LedgerEntryType entry_type = 5;
}

message SetAccountBalanceResponse {
}

message SetAccountCapitalRequirementRequest {
    //Account name
    string name = 1;

    //Fiat currency
    string currency = 2;

    //Capital requirement to set
    string capital_requirement = 3;

    //Description for why the capital requirement was set
    string description = 4;

    //Currency used for the capital requirement. The exchange rate between the base currency and the alternate currency is used to calculate the actual capital requirement.
    string alternate_capital_requirement_currency = 5;
}

message SetAccountCapitalRequirementResponse {
}

message SetAccountSecurityBalanceRequest {
    //Account name
    string name = 1;

    //Fiat currency
    string currency = 2;

    //Security ID, which may be a CUSIP, market symbol, or similar unique identifier
    string security_id = 3;

    //Balance to set
    string balance = 4;

    //Description for why the security balance was set
    string description = 5;
}

message SetAccountSecurityBalanceResponse {
}

message SetAccountSecurityMarketValueRequest {
    //Account name
    string name = 1;

    //Fiat currency
    string currency = 2;

    //Security ID, which may be a CUSIP, market symbol, or similar unique identifier
    string security_id = 3;

    //Market value of a single unit of the security ID in terms of the base currency
    string market_value = 4;

    //Haircut value of a single unit of the security ID in units of basis points (100 basis points is 1%)
    int64 haircut = 5;

    //Description for why the security market value and haircut was set
    string description = 6;
}

message SetAccountSecurityMarketValueResponse {
}

message ListSecurityDefinitionsRequest {
    //An optional subset of security identifiers to fetch for descriptions, returns all known values if left blank
    repeated string definitions = 1;
}

message ListSecurityDefinitionsResponse {
    //A set of security definitions keyed by the security identifier
    map<string, SecurityDefinition> definitions = 1;
}

message SetSecurityDefinitionRequest {
    //The security definition to store
    SecurityDefinition definition = 1;
}

message SetSecurityDefinitionResponse {}

message CreatePendingWithdrawalRequest {
    //Account name.
    string name = 1;

    //Fiat currency.
    string currency = 2;

    //Balance to withdraw.
    string balance = 3;

    //Description for the withdrawal request.
    string description = 4;

    //Bank details of the recipient.
    firms.v1beta1.BankDetails bank_recipient = 5;

    //Security ID.
    string security_id = 6;
}

message CreatePendingWithdrawalResponse {
    //The newly created pending withdrawal.
    PendingWithdrawal withdrawal = 1;
}

message ListPendingWithdrawalsRequest {
    //Filter by the account name.
    string name = 1;

    //Filter by the type of withdrawal.
    WithdrawalType withdrawal_type = 2;
}

message ListPendingWithdrawalsResponse {
    //List of pending withdrawals for the account.
    repeated PendingWithdrawal withdrawals = 1;
}

message GetPendingWithdrawalRequest {
    //Unique identifier for the withdrawal.
    string id = 1;
}

message GetPendingWithdrawalResponse {
    //The newly created pending withdrawal.
    PendingWithdrawal withdrawal = 1;
}

message DeletePendingWithdrawalRequest {
    //Unique identifier for the withdrawal.
    string id = 1;
}

message DeletePendingWithdrawalResponse {}

message AcknowledgePendingWithdrawalRequest {
    //Unique identifier for the withdrawal.
    string id = 1;
}

message AcknowledgePendingWithdrawalResponse {}

message ApprovePendingWithdrawalRequest {
    //Unique identifier for the withdrawal.
    string id = 1;
}

message ApprovePendingWithdrawalResponse {}

message CreatePendingCreditRequest {
    //Account name.
    string name = 1;

    //Fiat currency.
    string currency = 2;

    //If the pending credit is approved, the account's fiat balance will be increased by this amount.
    string balance = 3;

    //If the pending credit is approved, this is the description that EP3 will include on the Balance Ledger entry that it creates due to the change in the account's fiat balance.
    string description = 4;

    //An optional identifier of the pending credit to filter upon that also supports regexes.
    string correlation_id = 5;

    //If the pending credit is approved, this is the ledger entry type that EP3 will include on the Balance Ledger entry that it creates due to the change in the account's fiat balance.
    type.v1beta1.LedgerEntryType entry_type = 6;
}

message CreatePendingCreditResponse {
    //The newly created pending credit.
    PendingCredit credit = 1;
}

message ListPendingCreditsRequest {
    //A filter for account name.
    string name = 1;

    //A filter for the correlation identifier. Supports regexes.
    string correlation_id = 2;

    // Start time for the pending credits to be fetched.
    google.protobuf.Timestamp start_time = 3;

    // End time for the pending credits to be fetched.
    google.protobuf.Timestamp end_time = 4;
}

message ListPendingCreditsResponse {
    //List of pending credits returned by the given query filter.
    repeated PendingCredit credits = 1;
}

message GetPendingCreditRequest {
    //Unique identifier for the credit.
    string id = 1;
}

message GetPendingCreditResponse {
    //The newly created pending credit.
    PendingCredit credit = 1;
}

message DeletePendingCreditsRequest {
    //Unique identifier for a single credit.
    string id = 1;

    //A filter for the correlation identifier. Supports regexes.
    string correlation_id = 2;
}

message DeletePendingCreditsResponse {}

message ApprovePendingCreditsRequest {
    //Unique identifier for a single credit.
    string id = 1;

    //A filter for the correlation identifier. Supports regexes.
    string correlation_id = 2;

    // Start time for the pending credits to be approved.
    google.protobuf.Timestamp start_time = 3;

    // End time for the pending credits to be approved.
    google.protobuf.Timestamp end_time = 4;
}

message ApprovePendingCreditsResponse {
    // Number of approved pending credits.
    int64 number_approved = 1;
}

message ListPendingSettlementRequest {
    //A filter for account name. If left blank, list all pending settlements.
    string name = 1;
}

message ListPendingSettlementResponse {
    //List of pending settlements for each of the account names specified in the request.
    repeated PendingSettlement pending_settlements = 1;

    //Totals of all pending settlements per account in the request.
    repeated PendingSettlement total_pending_settlements = 2;
}
