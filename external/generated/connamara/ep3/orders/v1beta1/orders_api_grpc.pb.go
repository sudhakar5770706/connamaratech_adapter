// Copyright (c) 2022 Connamara Technologies, Inc.
//
// This file is distributed under the terms of the Connamara EP3 Software License Agreement.
//
// The above copyright notice and license notice shall be included in all copies or substantial portions of the Software.

// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v4.25.3
// source: connamara/ep3/orders/v1beta1/orders_api.proto

package ordersv1beta1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	OrdersAPI_SearchOrders_FullMethodName     = "/connamara.ep3.orders.v1beta1.OrdersAPI/SearchOrders"
	OrdersAPI_StreamOrders_FullMethodName     = "/connamara.ep3.orders.v1beta1.OrdersAPI/StreamOrders"
	OrdersAPI_SearchExecutions_FullMethodName = "/connamara.ep3.orders.v1beta1.OrdersAPI/SearchExecutions"
	OrdersAPI_StreamExecutions_FullMethodName = "/connamara.ep3.orders.v1beta1.OrdersAPI/StreamExecutions"
)

// OrdersAPIClient is the client API for OrdersAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type OrdersAPIClient interface {
	// SearchOrders returns a list of orders given request criteria.
	SearchOrders(ctx context.Context, in *SearchOrdersRequest, opts ...grpc.CallOption) (*SearchOrdersResponse, error)
	// StreamOrders streams a list of orders given request criteria.
	StreamOrders(ctx context.Context, in *StreamOrdersRequest, opts ...grpc.CallOption) (OrdersAPI_StreamOrdersClient, error)
	// SearchExecutions returns a list of executions given request criteria.
	SearchExecutions(ctx context.Context, in *SearchExecutionsRequest, opts ...grpc.CallOption) (*SearchExecutionsResponse, error)
	// StreamExecutions streams a list of executions given request criteria.
	StreamExecutions(ctx context.Context, in *StreamExecutionsRequest, opts ...grpc.CallOption) (OrdersAPI_StreamExecutionsClient, error)
}

type ordersAPIClient struct {
	cc grpc.ClientConnInterface
}

func NewOrdersAPIClient(cc grpc.ClientConnInterface) OrdersAPIClient {
	return &ordersAPIClient{cc}
}

func (c *ordersAPIClient) SearchOrders(ctx context.Context, in *SearchOrdersRequest, opts ...grpc.CallOption) (*SearchOrdersResponse, error) {
	out := new(SearchOrdersResponse)
	err := c.cc.Invoke(ctx, OrdersAPI_SearchOrders_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *ordersAPIClient) StreamOrders(ctx context.Context, in *StreamOrdersRequest, opts ...grpc.CallOption) (OrdersAPI_StreamOrdersClient, error) {
	stream, err := c.cc.NewStream(ctx, &OrdersAPI_ServiceDesc.Streams[0], OrdersAPI_StreamOrders_FullMethodName, opts...)
	if err != nil {
		return nil, err
	}
	x := &ordersAPIStreamOrdersClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type OrdersAPI_StreamOrdersClient interface {
	Recv() (*StreamOrdersResponse, error)
	grpc.ClientStream
}

type ordersAPIStreamOrdersClient struct {
	grpc.ClientStream
}

func (x *ordersAPIStreamOrdersClient) Recv() (*StreamOrdersResponse, error) {
	m := new(StreamOrdersResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *ordersAPIClient) SearchExecutions(ctx context.Context, in *SearchExecutionsRequest, opts ...grpc.CallOption) (*SearchExecutionsResponse, error) {
	out := new(SearchExecutionsResponse)
	err := c.cc.Invoke(ctx, OrdersAPI_SearchExecutions_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *ordersAPIClient) StreamExecutions(ctx context.Context, in *StreamExecutionsRequest, opts ...grpc.CallOption) (OrdersAPI_StreamExecutionsClient, error) {
	stream, err := c.cc.NewStream(ctx, &OrdersAPI_ServiceDesc.Streams[1], OrdersAPI_StreamExecutions_FullMethodName, opts...)
	if err != nil {
		return nil, err
	}
	x := &ordersAPIStreamExecutionsClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type OrdersAPI_StreamExecutionsClient interface {
	Recv() (*StreamExecutionsResponse, error)
	grpc.ClientStream
}

type ordersAPIStreamExecutionsClient struct {
	grpc.ClientStream
}

func (x *ordersAPIStreamExecutionsClient) Recv() (*StreamExecutionsResponse, error) {
	m := new(StreamExecutionsResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// OrdersAPIServer is the server API for OrdersAPI service.
// All implementations must embed UnimplementedOrdersAPIServer
// for forward compatibility
type OrdersAPIServer interface {
	// SearchOrders returns a list of orders given request criteria.
	SearchOrders(context.Context, *SearchOrdersRequest) (*SearchOrdersResponse, error)
	// StreamOrders streams a list of orders given request criteria.
	StreamOrders(*StreamOrdersRequest, OrdersAPI_StreamOrdersServer) error
	// SearchExecutions returns a list of executions given request criteria.
	SearchExecutions(context.Context, *SearchExecutionsRequest) (*SearchExecutionsResponse, error)
	// StreamExecutions streams a list of executions given request criteria.
	StreamExecutions(*StreamExecutionsRequest, OrdersAPI_StreamExecutionsServer) error
	mustEmbedUnimplementedOrdersAPIServer()
}

// UnimplementedOrdersAPIServer must be embedded to have forward compatible implementations.
type UnimplementedOrdersAPIServer struct {
}

func (UnimplementedOrdersAPIServer) SearchOrders(context.Context, *SearchOrdersRequest) (*SearchOrdersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchOrders not implemented")
}
func (UnimplementedOrdersAPIServer) StreamOrders(*StreamOrdersRequest, OrdersAPI_StreamOrdersServer) error {
	return status.Errorf(codes.Unimplemented, "method StreamOrders not implemented")
}
func (UnimplementedOrdersAPIServer) SearchExecutions(context.Context, *SearchExecutionsRequest) (*SearchExecutionsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchExecutions not implemented")
}
func (UnimplementedOrdersAPIServer) StreamExecutions(*StreamExecutionsRequest, OrdersAPI_StreamExecutionsServer) error {
	return status.Errorf(codes.Unimplemented, "method StreamExecutions not implemented")
}
func (UnimplementedOrdersAPIServer) mustEmbedUnimplementedOrdersAPIServer() {}

// UnsafeOrdersAPIServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to OrdersAPIServer will
// result in compilation errors.
type UnsafeOrdersAPIServer interface {
	mustEmbedUnimplementedOrdersAPIServer()
}

func RegisterOrdersAPIServer(s grpc.ServiceRegistrar, srv OrdersAPIServer) {
	s.RegisterService(&OrdersAPI_ServiceDesc, srv)
}

func _OrdersAPI_SearchOrders_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchOrdersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrdersAPIServer).SearchOrders(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: OrdersAPI_SearchOrders_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrdersAPIServer).SearchOrders(ctx, req.(*SearchOrdersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrdersAPI_StreamOrders_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(StreamOrdersRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(OrdersAPIServer).StreamOrders(m, &ordersAPIStreamOrdersServer{stream})
}

type OrdersAPI_StreamOrdersServer interface {
	Send(*StreamOrdersResponse) error
	grpc.ServerStream
}

type ordersAPIStreamOrdersServer struct {
	grpc.ServerStream
}

func (x *ordersAPIStreamOrdersServer) Send(m *StreamOrdersResponse) error {
	return x.ServerStream.SendMsg(m)
}

func _OrdersAPI_SearchExecutions_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchExecutionsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrdersAPIServer).SearchExecutions(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: OrdersAPI_SearchExecutions_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrdersAPIServer).SearchExecutions(ctx, req.(*SearchExecutionsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrdersAPI_StreamExecutions_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(StreamExecutionsRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(OrdersAPIServer).StreamExecutions(m, &ordersAPIStreamExecutionsServer{stream})
}

type OrdersAPI_StreamExecutionsServer interface {
	Send(*StreamExecutionsResponse) error
	grpc.ServerStream
}

type ordersAPIStreamExecutionsServer struct {
	grpc.ServerStream
}

func (x *ordersAPIStreamExecutionsServer) Send(m *StreamExecutionsResponse) error {
	return x.ServerStream.SendMsg(m)
}

// OrdersAPI_ServiceDesc is the grpc.ServiceDesc for OrdersAPI service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var OrdersAPI_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "connamara.ep3.orders.v1beta1.OrdersAPI",
	HandlerType: (*OrdersAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SearchOrders",
			Handler:    _OrdersAPI_SearchOrders_Handler,
		},
		{
			MethodName: "SearchExecutions",
			Handler:    _OrdersAPI_SearchExecutions_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "StreamOrders",
			Handler:       _OrdersAPI_StreamOrders_Handler,
			ServerStreams: true,
		},
		{
			StreamName:    "StreamExecutions",
			Handler:       _OrdersAPI_StreamExecutions_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "connamara/ep3/orders/v1beta1/orders_api.proto",
}
