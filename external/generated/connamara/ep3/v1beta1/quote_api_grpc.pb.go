// Copyright (c) 2022 Connamara Technologies, Inc.
//
// This file is distributed under the terms of the Connamara EP3 Software License Agreement.
//
// The above copyright notice and license notice shall be included in all copies or substantial portions of the Software.

// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v4.25.3
// source: connamara/ep3/v1beta1/quote_api.proto

package ep3v1beta1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	QuoteAPI_SearchRequestForQuotes_FullMethodName = "/connamara.ep3.v1beta1.QuoteAPI/SearchRequestForQuotes"
	QuoteAPI_SearchQuotes_FullMethodName           = "/connamara.ep3.v1beta1.QuoteAPI/SearchQuotes"
)

// QuoteAPIClient is the client API for QuoteAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type QuoteAPIClient interface {
	// SearchRequestForQuotes lists all request for quotes matching the request.
	SearchRequestForQuotes(ctx context.Context, in *SearchRequestForQuotesRequest, opts ...grpc.CallOption) (*SearchRequestForQuotesResponse, error)
	// SearchQuotes lists all quotes matching the request.
	SearchQuotes(ctx context.Context, in *SearchQuotesRequest, opts ...grpc.CallOption) (*SearchQuotesResponse, error)
}

type quoteAPIClient struct {
	cc grpc.ClientConnInterface
}

func NewQuoteAPIClient(cc grpc.ClientConnInterface) QuoteAPIClient {
	return &quoteAPIClient{cc}
}

func (c *quoteAPIClient) SearchRequestForQuotes(ctx context.Context, in *SearchRequestForQuotesRequest, opts ...grpc.CallOption) (*SearchRequestForQuotesResponse, error) {
	out := new(SearchRequestForQuotesResponse)
	err := c.cc.Invoke(ctx, QuoteAPI_SearchRequestForQuotes_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *quoteAPIClient) SearchQuotes(ctx context.Context, in *SearchQuotesRequest, opts ...grpc.CallOption) (*SearchQuotesResponse, error) {
	out := new(SearchQuotesResponse)
	err := c.cc.Invoke(ctx, QuoteAPI_SearchQuotes_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// QuoteAPIServer is the server API for QuoteAPI service.
// All implementations must embed UnimplementedQuoteAPIServer
// for forward compatibility
type QuoteAPIServer interface {
	// SearchRequestForQuotes lists all request for quotes matching the request.
	SearchRequestForQuotes(context.Context, *SearchRequestForQuotesRequest) (*SearchRequestForQuotesResponse, error)
	// SearchQuotes lists all quotes matching the request.
	SearchQuotes(context.Context, *SearchQuotesRequest) (*SearchQuotesResponse, error)
	mustEmbedUnimplementedQuoteAPIServer()
}

// UnimplementedQuoteAPIServer must be embedded to have forward compatible implementations.
type UnimplementedQuoteAPIServer struct {
}

func (UnimplementedQuoteAPIServer) SearchRequestForQuotes(context.Context, *SearchRequestForQuotesRequest) (*SearchRequestForQuotesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchRequestForQuotes not implemented")
}
func (UnimplementedQuoteAPIServer) SearchQuotes(context.Context, *SearchQuotesRequest) (*SearchQuotesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchQuotes not implemented")
}
func (UnimplementedQuoteAPIServer) mustEmbedUnimplementedQuoteAPIServer() {}

// UnsafeQuoteAPIServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to QuoteAPIServer will
// result in compilation errors.
type UnsafeQuoteAPIServer interface {
	mustEmbedUnimplementedQuoteAPIServer()
}

func RegisterQuoteAPIServer(s grpc.ServiceRegistrar, srv QuoteAPIServer) {
	s.RegisterService(&QuoteAPI_ServiceDesc, srv)
}

func _QuoteAPI_SearchRequestForQuotes_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchRequestForQuotesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuoteAPIServer).SearchRequestForQuotes(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: QuoteAPI_SearchRequestForQuotes_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuoteAPIServer).SearchRequestForQuotes(ctx, req.(*SearchRequestForQuotesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _QuoteAPI_SearchQuotes_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchQuotesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuoteAPIServer).SearchQuotes(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: QuoteAPI_SearchQuotes_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuoteAPIServer).SearchQuotes(ctx, req.(*SearchQuotesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// QuoteAPI_ServiceDesc is the grpc.ServiceDesc for QuoteAPI service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var QuoteAPI_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "connamara.ep3.v1beta1.QuoteAPI",
	HandlerType: (*QuoteAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SearchRequestForQuotes",
			Handler:    _QuoteAPI_SearchRequestForQuotes_Handler,
		},
		{
			MethodName: "SearchQuotes",
			Handler:    _QuoteAPI_SearchQuotes_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "connamara/ep3/v1beta1/quote_api.proto",
}
