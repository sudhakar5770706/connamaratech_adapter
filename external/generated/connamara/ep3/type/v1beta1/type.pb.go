// Copyright (c) 2022 Connamara Technologies, Inc.
//
// This file is distributed under the terms of the Connamara EP3 Software License Agreement.
//
// The above copyright notice and license notice shall be included in all copies or substantial portions of the Software.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.32.0
// 	protoc        v4.25.3
// source: connamara/ep3/type/v1beta1/type.proto

package typev1beta1

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// DayOfWeek represents a day of week.
type DayOfWeek int32

const (
	// The unspecified day-of-week.
	DayOfWeek_DAY_OF_WEEK_UNSPECIFIED DayOfWeek = 0
	// The day-of-week of Monday.
	DayOfWeek_DAY_OF_WEEK_MONDAY DayOfWeek = 1
	// The day-of-week of Tuesday.
	DayOfWeek_DAY_OF_WEEK_TUESDAY DayOfWeek = 2
	// The day-of-week of Wednesday.
	DayOfWeek_DAY_OF_WEEK_WEDNESDAY DayOfWeek = 3
	// The day-of-week of Thursday.
	DayOfWeek_DAY_OF_WEEK_THURSDAY DayOfWeek = 4
	// The day-of-week of Friday.
	DayOfWeek_DAY_OF_WEEK_FRIDAY DayOfWeek = 5
	// The day-of-week of Saturday.
	DayOfWeek_DAY_OF_WEEK_SATURDAY DayOfWeek = 6
	// The day-of-week of Sunday.
	DayOfWeek_DAY_OF_WEEK_SUNDAY DayOfWeek = 7
)

// Enum value maps for DayOfWeek.
var (
	DayOfWeek_name = map[int32]string{
		0: "DAY_OF_WEEK_UNSPECIFIED",
		1: "DAY_OF_WEEK_MONDAY",
		2: "DAY_OF_WEEK_TUESDAY",
		3: "DAY_OF_WEEK_WEDNESDAY",
		4: "DAY_OF_WEEK_THURSDAY",
		5: "DAY_OF_WEEK_FRIDAY",
		6: "DAY_OF_WEEK_SATURDAY",
		7: "DAY_OF_WEEK_SUNDAY",
	}
	DayOfWeek_value = map[string]int32{
		"DAY_OF_WEEK_UNSPECIFIED": 0,
		"DAY_OF_WEEK_MONDAY":      1,
		"DAY_OF_WEEK_TUESDAY":     2,
		"DAY_OF_WEEK_WEDNESDAY":   3,
		"DAY_OF_WEEK_THURSDAY":    4,
		"DAY_OF_WEEK_FRIDAY":      5,
		"DAY_OF_WEEK_SATURDAY":    6,
		"DAY_OF_WEEK_SUNDAY":      7,
	}
)

func (x DayOfWeek) Enum() *DayOfWeek {
	p := new(DayOfWeek)
	*p = x
	return p
}

func (x DayOfWeek) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (DayOfWeek) Descriptor() protoreflect.EnumDescriptor {
	return file_connamara_ep3_type_v1beta1_type_proto_enumTypes[0].Descriptor()
}

func (DayOfWeek) Type() protoreflect.EnumType {
	return &file_connamara_ep3_type_v1beta1_type_proto_enumTypes[0]
}

func (x DayOfWeek) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use DayOfWeek.Descriptor instead.
func (DayOfWeek) EnumDescriptor() ([]byte, []int) {
	return file_connamara_ep3_type_v1beta1_type_proto_rawDescGZIP(), []int{0}
}

// LedgerEntryType is a code to identify the classification of an entry to the ledger.
type LedgerEntryType int32

const (
	// Undefined state of the ledger entry type
	LedgerEntryType_LEDGER_ENTRY_TYPE_UNDEFINED LedgerEntryType = 0
	// Ledger entry is due to a deposit of fiat currency
	LedgerEntryType_LEDGER_ENTRY_TYPE_DEPOSIT LedgerEntryType = 1
	// Ledger entry is due to a withdrawal of fiat currency
	LedgerEntryType_LEDGER_ENTRY_TYPE_WITHDRAWAL LedgerEntryType = 2
	// Ledger entry is due to the execution of an order
	LedgerEntryType_LEDGER_ENTRY_TYPE_ORDER_EXECUTION LedgerEntryType = 3
	// Ledger entry reflects the reversal of a previous entry because of a trade bust or other such error correction
	LedgerEntryType_LEDGER_ENTRY_TYPE_CORRECTION LedgerEntryType = 4
	// Ledger entry is due to netting of positions
	LedgerEntryType_LEDGER_ENTRY_TYPE_NETTING LedgerEntryType = 5
	// Ledger entry is due to resolution of a contract
	LedgerEntryType_LEDGER_ENTRY_TYPE_RESOLUTION LedgerEntryType = 6
	// Ledger entry is due to a manual adjustment typically by an exchange super user administrator
	LedgerEntryType_LEDGER_ENTRY_TYPE_MANUAL_ADJUSTMENT LedgerEntryType = 7
	// Ledger entry is due to the balance adjustment of securities
	LedgerEntryType_LEDGER_ENTRY_TYPE_SECURITY_BALANCE_ADJUSTMENT LedgerEntryType = 8
	// Ledger entry is due to changing market value of securities
	LedgerEntryType_LEDGER_ENTRY_TYPE_SECURITY_MARK_TO_MARKET LedgerEntryType = 9
	// Ledger entry is due to changing account properties
	LedgerEntryType_LEDGER_ENTRY_TYPE_ACCOUNT_PROPERTY_ADJUSTMENT LedgerEntryType = 10
	// Ledger entry is due to collection of commissions
	LedgerEntryType_LEDGER_ENTRY_TYPE_COMMISSION LedgerEntryType = 11
	// Ledger entry is due to expiration of a contract
	LedgerEntryType_LEDGER_ENTRY_TYPE_CONTRACT_EXPIRATION LedgerEntryType = 12
	// Ledger entry is due to the adjustment of pending credits
	LedgerEntryType_LEDGER_ENTRY_TYPE_PENDING_CREDIT_ADJUSTMENT LedgerEntryType = 13
	// Ledger entry is due to the change of a book from a closed state to an open state
	LedgerEntryType_LEDGER_ENTRY_TYPE_BEGINNING_OF_DAY LedgerEntryType = 14
	// Ledger entry is due to a withdrawal of a security
	LedgerEntryType_LEDGER_ENTRY_TYPE_SECURITY_WITHDRAWAL LedgerEntryType = 15
)

// Enum value maps for LedgerEntryType.
var (
	LedgerEntryType_name = map[int32]string{
		0:  "LEDGER_ENTRY_TYPE_UNDEFINED",
		1:  "LEDGER_ENTRY_TYPE_DEPOSIT",
		2:  "LEDGER_ENTRY_TYPE_WITHDRAWAL",
		3:  "LEDGER_ENTRY_TYPE_ORDER_EXECUTION",
		4:  "LEDGER_ENTRY_TYPE_CORRECTION",
		5:  "LEDGER_ENTRY_TYPE_NETTING",
		6:  "LEDGER_ENTRY_TYPE_RESOLUTION",
		7:  "LEDGER_ENTRY_TYPE_MANUAL_ADJUSTMENT",
		8:  "LEDGER_ENTRY_TYPE_SECURITY_BALANCE_ADJUSTMENT",
		9:  "LEDGER_ENTRY_TYPE_SECURITY_MARK_TO_MARKET",
		10: "LEDGER_ENTRY_TYPE_ACCOUNT_PROPERTY_ADJUSTMENT",
		11: "LEDGER_ENTRY_TYPE_COMMISSION",
		12: "LEDGER_ENTRY_TYPE_CONTRACT_EXPIRATION",
		13: "LEDGER_ENTRY_TYPE_PENDING_CREDIT_ADJUSTMENT",
		14: "LEDGER_ENTRY_TYPE_BEGINNING_OF_DAY",
		15: "LEDGER_ENTRY_TYPE_SECURITY_WITHDRAWAL",
	}
	LedgerEntryType_value = map[string]int32{
		"LEDGER_ENTRY_TYPE_UNDEFINED":                   0,
		"LEDGER_ENTRY_TYPE_DEPOSIT":                     1,
		"LEDGER_ENTRY_TYPE_WITHDRAWAL":                  2,
		"LEDGER_ENTRY_TYPE_ORDER_EXECUTION":             3,
		"LEDGER_ENTRY_TYPE_CORRECTION":                  4,
		"LEDGER_ENTRY_TYPE_NETTING":                     5,
		"LEDGER_ENTRY_TYPE_RESOLUTION":                  6,
		"LEDGER_ENTRY_TYPE_MANUAL_ADJUSTMENT":           7,
		"LEDGER_ENTRY_TYPE_SECURITY_BALANCE_ADJUSTMENT": 8,
		"LEDGER_ENTRY_TYPE_SECURITY_MARK_TO_MARKET":     9,
		"LEDGER_ENTRY_TYPE_ACCOUNT_PROPERTY_ADJUSTMENT": 10,
		"LEDGER_ENTRY_TYPE_COMMISSION":                  11,
		"LEDGER_ENTRY_TYPE_CONTRACT_EXPIRATION":         12,
		"LEDGER_ENTRY_TYPE_PENDING_CREDIT_ADJUSTMENT":   13,
		"LEDGER_ENTRY_TYPE_BEGINNING_OF_DAY":            14,
		"LEDGER_ENTRY_TYPE_SECURITY_WITHDRAWAL":         15,
	}
)

func (x LedgerEntryType) Enum() *LedgerEntryType {
	p := new(LedgerEntryType)
	*p = x
	return p
}

func (x LedgerEntryType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (LedgerEntryType) Descriptor() protoreflect.EnumDescriptor {
	return file_connamara_ep3_type_v1beta1_type_proto_enumTypes[1].Descriptor()
}

func (LedgerEntryType) Type() protoreflect.EnumType {
	return &file_connamara_ep3_type_v1beta1_type_proto_enumTypes[1]
}

func (x LedgerEntryType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use LedgerEntryType.Descriptor instead.
func (LedgerEntryType) EnumDescriptor() ([]byte, []int) {
	return file_connamara_ep3_type_v1beta1_type_proto_rawDescGZIP(), []int{1}
}

// Date represents a whole or partial calendar date.
type Date struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Year of date. Must be from 1 to 9999, or 0 if specifying a date without
	// a year.
	Year int32 `protobuf:"varint,1,opt,name=year,proto3" json:"year,omitempty"`
	// Month of year. Must be from 1 to 12, or 0 if specifying a year without a
	// month and day.
	Month int32 `protobuf:"varint,2,opt,name=month,proto3" json:"month,omitempty"`
	// Day of month. Must be from 1 to 31 and valid for the year and month, or 0
	// if specifying a year by itself or a year and month where the day is not
	// significant.
	Day int32 `protobuf:"varint,3,opt,name=day,proto3" json:"day,omitempty"`
}

func (x *Date) Reset() {
	*x = Date{}
	if protoimpl.UnsafeEnabled {
		mi := &file_connamara_ep3_type_v1beta1_type_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Date) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Date) ProtoMessage() {}

func (x *Date) ProtoReflect() protoreflect.Message {
	mi := &file_connamara_ep3_type_v1beta1_type_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Date.ProtoReflect.Descriptor instead.
func (*Date) Descriptor() ([]byte, []int) {
	return file_connamara_ep3_type_v1beta1_type_proto_rawDescGZIP(), []int{0}
}

func (x *Date) GetYear() int32 {
	if x != nil {
		return x.Year
	}
	return 0
}

func (x *Date) GetMonth() int32 {
	if x != nil {
		return x.Month
	}
	return 0
}

func (x *Date) GetDay() int32 {
	if x != nil {
		return x.Day
	}
	return 0
}

// TimeOfDay represents a time of day. The date and time zone are either not significant
// or are specified elsewhere. Related types are [google.type.Date][google.type.Date]
// and `google.protobuf.Timestamp`.
type TimeOfDay struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Hours of day in 24 hour format. Must be from 0 to 23.
	Hours int32 `protobuf:"varint,1,opt,name=hours,proto3" json:"hours,omitempty"`
	// Minutes of hour of day. Must be from 0 to 59.
	Minutes int32 `protobuf:"varint,2,opt,name=minutes,proto3" json:"minutes,omitempty"`
	// Seconds of minutes of the time. Must be from 0 to 59.
	Seconds int32 `protobuf:"varint,3,opt,name=seconds,proto3" json:"seconds,omitempty"`
}

func (x *TimeOfDay) Reset() {
	*x = TimeOfDay{}
	if protoimpl.UnsafeEnabled {
		mi := &file_connamara_ep3_type_v1beta1_type_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TimeOfDay) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TimeOfDay) ProtoMessage() {}

func (x *TimeOfDay) ProtoReflect() protoreflect.Message {
	mi := &file_connamara_ep3_type_v1beta1_type_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TimeOfDay.ProtoReflect.Descriptor instead.
func (*TimeOfDay) Descriptor() ([]byte, []int) {
	return file_connamara_ep3_type_v1beta1_type_proto_rawDescGZIP(), []int{1}
}

func (x *TimeOfDay) GetHours() int32 {
	if x != nil {
		return x.Hours
	}
	return 0
}

func (x *TimeOfDay) GetMinutes() int32 {
	if x != nil {
		return x.Minutes
	}
	return 0
}

func (x *TimeOfDay) GetSeconds() int32 {
	if x != nil {
		return x.Seconds
	}
	return 0
}

var File_connamara_ep3_type_v1beta1_type_proto protoreflect.FileDescriptor

var file_connamara_ep3_type_v1beta1_type_proto_rawDesc = []byte{
	0x0a, 0x25, 0x63, 0x6f, 0x6e, 0x6e, 0x61, 0x6d, 0x61, 0x72, 0x61, 0x2f, 0x65, 0x70, 0x33, 0x2f,
	0x74, 0x79, 0x70, 0x65, 0x2f, 0x76, 0x31, 0x62, 0x65, 0x74, 0x61, 0x31, 0x2f, 0x74, 0x79, 0x70,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1a, 0x63, 0x6f, 0x6e, 0x6e, 0x61, 0x6d, 0x61,
	0x72, 0x61, 0x2e, 0x65, 0x70, 0x33, 0x2e, 0x74, 0x79, 0x70, 0x65, 0x2e, 0x76, 0x31, 0x62, 0x65,
	0x74, 0x61, 0x31, 0x22, 0x42, 0x0a, 0x04, 0x44, 0x61, 0x74, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x79,
	0x65, 0x61, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x79, 0x65, 0x61, 0x72, 0x12,
	0x14, 0x0a, 0x05, 0x6d, 0x6f, 0x6e, 0x74, 0x68, 0x18, 0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05,
	0x6d, 0x6f, 0x6e, 0x74, 0x68, 0x12, 0x10, 0x0a, 0x03, 0x64, 0x61, 0x79, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x03, 0x64, 0x61, 0x79, 0x22, 0x55, 0x0a, 0x09, 0x54, 0x69, 0x6d, 0x65, 0x4f,
	0x66, 0x44, 0x61, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x68, 0x6f, 0x75, 0x72, 0x73, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x05, 0x68, 0x6f, 0x75, 0x72, 0x73, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x69,
	0x6e, 0x75, 0x74, 0x65, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x07, 0x6d, 0x69, 0x6e,
	0x75, 0x74, 0x65, 0x73, 0x12, 0x18, 0x0a, 0x07, 0x73, 0x65, 0x63, 0x6f, 0x6e, 0x64, 0x73, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x07, 0x73, 0x65, 0x63, 0x6f, 0x6e, 0x64, 0x73, 0x2a, 0xd8,
	0x01, 0x0a, 0x09, 0x44, 0x61, 0x79, 0x4f, 0x66, 0x57, 0x65, 0x65, 0x6b, 0x12, 0x1b, 0x0a, 0x17,
	0x44, 0x41, 0x59, 0x5f, 0x4f, 0x46, 0x5f, 0x57, 0x45, 0x45, 0x4b, 0x5f, 0x55, 0x4e, 0x53, 0x50,
	0x45, 0x43, 0x49, 0x46, 0x49, 0x45, 0x44, 0x10, 0x00, 0x12, 0x16, 0x0a, 0x12, 0x44, 0x41, 0x59,
	0x5f, 0x4f, 0x46, 0x5f, 0x57, 0x45, 0x45, 0x4b, 0x5f, 0x4d, 0x4f, 0x4e, 0x44, 0x41, 0x59, 0x10,
	0x01, 0x12, 0x17, 0x0a, 0x13, 0x44, 0x41, 0x59, 0x5f, 0x4f, 0x46, 0x5f, 0x57, 0x45, 0x45, 0x4b,
	0x5f, 0x54, 0x55, 0x45, 0x53, 0x44, 0x41, 0x59, 0x10, 0x02, 0x12, 0x19, 0x0a, 0x15, 0x44, 0x41,
	0x59, 0x5f, 0x4f, 0x46, 0x5f, 0x57, 0x45, 0x45, 0x4b, 0x5f, 0x57, 0x45, 0x44, 0x4e, 0x45, 0x53,
	0x44, 0x41, 0x59, 0x10, 0x03, 0x12, 0x18, 0x0a, 0x14, 0x44, 0x41, 0x59, 0x5f, 0x4f, 0x46, 0x5f,
	0x57, 0x45, 0x45, 0x4b, 0x5f, 0x54, 0x48, 0x55, 0x52, 0x53, 0x44, 0x41, 0x59, 0x10, 0x04, 0x12,
	0x16, 0x0a, 0x12, 0x44, 0x41, 0x59, 0x5f, 0x4f, 0x46, 0x5f, 0x57, 0x45, 0x45, 0x4b, 0x5f, 0x46,
	0x52, 0x49, 0x44, 0x41, 0x59, 0x10, 0x05, 0x12, 0x18, 0x0a, 0x14, 0x44, 0x41, 0x59, 0x5f, 0x4f,
	0x46, 0x5f, 0x57, 0x45, 0x45, 0x4b, 0x5f, 0x53, 0x41, 0x54, 0x55, 0x52, 0x44, 0x41, 0x59, 0x10,
	0x06, 0x12, 0x16, 0x0a, 0x12, 0x44, 0x41, 0x59, 0x5f, 0x4f, 0x46, 0x5f, 0x57, 0x45, 0x45, 0x4b,
	0x5f, 0x53, 0x55, 0x4e, 0x44, 0x41, 0x59, 0x10, 0x07, 0x2a, 0x8c, 0x05, 0x0a, 0x0f, 0x4c, 0x65,
	0x64, 0x67, 0x65, 0x72, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x54, 0x79, 0x70, 0x65, 0x12, 0x1f, 0x0a,
	0x1b, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59,
	0x50, 0x45, 0x5f, 0x55, 0x4e, 0x44, 0x45, 0x46, 0x49, 0x4e, 0x45, 0x44, 0x10, 0x00, 0x12, 0x1d,
	0x0a, 0x19, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54,
	0x59, 0x50, 0x45, 0x5f, 0x44, 0x45, 0x50, 0x4f, 0x53, 0x49, 0x54, 0x10, 0x01, 0x12, 0x20, 0x0a,
	0x1c, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59,
	0x50, 0x45, 0x5f, 0x57, 0x49, 0x54, 0x48, 0x44, 0x52, 0x41, 0x57, 0x41, 0x4c, 0x10, 0x02, 0x12,
	0x25, 0x0a, 0x21, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f,
	0x54, 0x59, 0x50, 0x45, 0x5f, 0x4f, 0x52, 0x44, 0x45, 0x52, 0x5f, 0x45, 0x58, 0x45, 0x43, 0x55,
	0x54, 0x49, 0x4f, 0x4e, 0x10, 0x03, 0x12, 0x20, 0x0a, 0x1c, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52,
	0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x43, 0x4f, 0x52, 0x52,
	0x45, 0x43, 0x54, 0x49, 0x4f, 0x4e, 0x10, 0x04, 0x12, 0x1d, 0x0a, 0x19, 0x4c, 0x45, 0x44, 0x47,
	0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x4e, 0x45,
	0x54, 0x54, 0x49, 0x4e, 0x47, 0x10, 0x05, 0x12, 0x20, 0x0a, 0x1c, 0x4c, 0x45, 0x44, 0x47, 0x45,
	0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x52, 0x45, 0x53,
	0x4f, 0x4c, 0x55, 0x54, 0x49, 0x4f, 0x4e, 0x10, 0x06, 0x12, 0x27, 0x0a, 0x23, 0x4c, 0x45, 0x44,
	0x47, 0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x4d,
	0x41, 0x4e, 0x55, 0x41, 0x4c, 0x5f, 0x41, 0x44, 0x4a, 0x55, 0x53, 0x54, 0x4d, 0x45, 0x4e, 0x54,
	0x10, 0x07, 0x12, 0x31, 0x0a, 0x2d, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54,
	0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x53, 0x45, 0x43, 0x55, 0x52, 0x49, 0x54, 0x59,
	0x5f, 0x42, 0x41, 0x4c, 0x41, 0x4e, 0x43, 0x45, 0x5f, 0x41, 0x44, 0x4a, 0x55, 0x53, 0x54, 0x4d,
	0x45, 0x4e, 0x54, 0x10, 0x08, 0x12, 0x2d, 0x0a, 0x29, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f,
	0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x53, 0x45, 0x43, 0x55, 0x52,
	0x49, 0x54, 0x59, 0x5f, 0x4d, 0x41, 0x52, 0x4b, 0x5f, 0x54, 0x4f, 0x5f, 0x4d, 0x41, 0x52, 0x4b,
	0x45, 0x54, 0x10, 0x09, 0x12, 0x31, 0x0a, 0x2d, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f, 0x45,
	0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x41, 0x43, 0x43, 0x4f, 0x55, 0x4e,
	0x54, 0x5f, 0x50, 0x52, 0x4f, 0x50, 0x45, 0x52, 0x54, 0x59, 0x5f, 0x41, 0x44, 0x4a, 0x55, 0x53,
	0x54, 0x4d, 0x45, 0x4e, 0x54, 0x10, 0x0a, 0x12, 0x20, 0x0a, 0x1c, 0x4c, 0x45, 0x44, 0x47, 0x45,
	0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x43, 0x4f, 0x4d,
	0x4d, 0x49, 0x53, 0x53, 0x49, 0x4f, 0x4e, 0x10, 0x0b, 0x12, 0x29, 0x0a, 0x25, 0x4c, 0x45, 0x44,
	0x47, 0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x43,
	0x4f, 0x4e, 0x54, 0x52, 0x41, 0x43, 0x54, 0x5f, 0x45, 0x58, 0x50, 0x49, 0x52, 0x41, 0x54, 0x49,
	0x4f, 0x4e, 0x10, 0x0c, 0x12, 0x2f, 0x0a, 0x2b, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f, 0x45,
	0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x50, 0x45, 0x4e, 0x44, 0x49, 0x4e,
	0x47, 0x5f, 0x43, 0x52, 0x45, 0x44, 0x49, 0x54, 0x5f, 0x41, 0x44, 0x4a, 0x55, 0x53, 0x54, 0x4d,
	0x45, 0x4e, 0x54, 0x10, 0x0d, 0x12, 0x26, 0x0a, 0x22, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f,
	0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x42, 0x45, 0x47, 0x49, 0x4e,
	0x4e, 0x49, 0x4e, 0x47, 0x5f, 0x4f, 0x46, 0x5f, 0x44, 0x41, 0x59, 0x10, 0x0e, 0x12, 0x29, 0x0a,
	0x25, 0x4c, 0x45, 0x44, 0x47, 0x45, 0x52, 0x5f, 0x45, 0x4e, 0x54, 0x52, 0x59, 0x5f, 0x54, 0x59,
	0x50, 0x45, 0x5f, 0x53, 0x45, 0x43, 0x55, 0x52, 0x49, 0x54, 0x59, 0x5f, 0x57, 0x49, 0x54, 0x48,
	0x44, 0x52, 0x41, 0x57, 0x41, 0x4c, 0x10, 0x0f, 0x42, 0x5f, 0x0a, 0x1e, 0x63, 0x6f, 0x6d, 0x2e,
	0x63, 0x6f, 0x6e, 0x6e, 0x61, 0x6d, 0x61, 0x72, 0x61, 0x2e, 0x65, 0x70, 0x33, 0x2e, 0x74, 0x79,
	0x70, 0x65, 0x2e, 0x76, 0x31, 0x62, 0x65, 0x74, 0x61, 0x31, 0x42, 0x09, 0x54, 0x79, 0x70, 0x65,
	0x50, 0x72, 0x6f, 0x74, 0x6f, 0x50, 0x01, 0x5a, 0x0d, 0x2e, 0x2f, 0x74, 0x79, 0x70, 0x65, 0x76,
	0x31, 0x62, 0x65, 0x74, 0x61, 0x31, 0xa2, 0x02, 0x03, 0x43, 0x45, 0x54, 0xaa, 0x02, 0x1a, 0x43,
	0x6f, 0x6e, 0x6e, 0x61, 0x6d, 0x61, 0x72, 0x61, 0x2e, 0x45, 0x70, 0x33, 0x2e, 0x54, 0x79, 0x70,
	0x65, 0x2e, 0x56, 0x31, 0x42, 0x65, 0x74, 0x61, 0x31, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x33,
}

var (
	file_connamara_ep3_type_v1beta1_type_proto_rawDescOnce sync.Once
	file_connamara_ep3_type_v1beta1_type_proto_rawDescData = file_connamara_ep3_type_v1beta1_type_proto_rawDesc
)

func file_connamara_ep3_type_v1beta1_type_proto_rawDescGZIP() []byte {
	file_connamara_ep3_type_v1beta1_type_proto_rawDescOnce.Do(func() {
		file_connamara_ep3_type_v1beta1_type_proto_rawDescData = protoimpl.X.CompressGZIP(file_connamara_ep3_type_v1beta1_type_proto_rawDescData)
	})
	return file_connamara_ep3_type_v1beta1_type_proto_rawDescData
}

var file_connamara_ep3_type_v1beta1_type_proto_enumTypes = make([]protoimpl.EnumInfo, 2)
var file_connamara_ep3_type_v1beta1_type_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_connamara_ep3_type_v1beta1_type_proto_goTypes = []interface{}{
	(DayOfWeek)(0),       // 0: connamara.ep3.type.v1beta1.DayOfWeek
	(LedgerEntryType)(0), // 1: connamara.ep3.type.v1beta1.LedgerEntryType
	(*Date)(nil),         // 2: connamara.ep3.type.v1beta1.Date
	(*TimeOfDay)(nil),    // 3: connamara.ep3.type.v1beta1.TimeOfDay
}
var file_connamara_ep3_type_v1beta1_type_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_connamara_ep3_type_v1beta1_type_proto_init() }
func file_connamara_ep3_type_v1beta1_type_proto_init() {
	if File_connamara_ep3_type_v1beta1_type_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_connamara_ep3_type_v1beta1_type_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Date); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_connamara_ep3_type_v1beta1_type_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TimeOfDay); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_connamara_ep3_type_v1beta1_type_proto_rawDesc,
			NumEnums:      2,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_connamara_ep3_type_v1beta1_type_proto_goTypes,
		DependencyIndexes: file_connamara_ep3_type_v1beta1_type_proto_depIdxs,
		EnumInfos:         file_connamara_ep3_type_v1beta1_type_proto_enumTypes,
		MessageInfos:      file_connamara_ep3_type_v1beta1_type_proto_msgTypes,
	}.Build()
	File_connamara_ep3_type_v1beta1_type_proto = out.File
	file_connamara_ep3_type_v1beta1_type_proto_rawDesc = nil
	file_connamara_ep3_type_v1beta1_type_proto_goTypes = nil
	file_connamara_ep3_type_v1beta1_type_proto_depIdxs = nil
}
