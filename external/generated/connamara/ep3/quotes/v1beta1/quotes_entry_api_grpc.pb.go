// Copyright (c) 2022 Connamara Technologies, Inc.
//
// This file is distributed under the terms of the Connamara EP3 Software License Agreement.
//
// The above copyright notice and license notice shall be included in all copies or substantial portions of the Software.

// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v4.25.3
// source: connamara/ep3/quotes/v1beta1/quotes_entry_api.proto

package quotesv1beta1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	QuotesEntryAPI_CreateRequestForQuote_FullMethodName = "/connamara.ep3.quotes.v1beta1.QuotesEntryAPI/CreateRequestForQuote"
	QuotesEntryAPI_CreateQuote_FullMethodName           = "/connamara.ep3.quotes.v1beta1.QuotesEntryAPI/CreateQuote"
	QuotesEntryAPI_CancelQuote_FullMethodName           = "/connamara.ep3.quotes.v1beta1.QuotesEntryAPI/CancelQuote"
	QuotesEntryAPI_PassQuote_FullMethodName             = "/connamara.ep3.quotes.v1beta1.QuotesEntryAPI/PassQuote"
)

// QuotesEntryAPIClient is the client API for QuotesEntryAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type QuotesEntryAPIClient interface {
	// CreateRequestForQuote creates a request for quote on the exchange.
	CreateRequestForQuote(ctx context.Context, in *CreateRequestForQuoteRequest, opts ...grpc.CallOption) (*CreateRequestForQuoteResponse, error)
	// CreateQuote creates a quote on the exchange.
	CreateQuote(ctx context.Context, in *CreateQuoteRequest, opts ...grpc.CallOption) (*CreateQuoteResponse, error)
	// CancelQuote cancels a quote on the exchange.
	CancelQuote(ctx context.Context, in *CancelQuoteRequest, opts ...grpc.CallOption) (*CancelQuoteResponse, error)
	// PassQuote passes on a quote from the exchange.
	PassQuote(ctx context.Context, in *PassQuoteRequest, opts ...grpc.CallOption) (*PassQuoteResponse, error)
}

type quotesEntryAPIClient struct {
	cc grpc.ClientConnInterface
}

func NewQuotesEntryAPIClient(cc grpc.ClientConnInterface) QuotesEntryAPIClient {
	return &quotesEntryAPIClient{cc}
}

func (c *quotesEntryAPIClient) CreateRequestForQuote(ctx context.Context, in *CreateRequestForQuoteRequest, opts ...grpc.CallOption) (*CreateRequestForQuoteResponse, error) {
	out := new(CreateRequestForQuoteResponse)
	err := c.cc.Invoke(ctx, QuotesEntryAPI_CreateRequestForQuote_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *quotesEntryAPIClient) CreateQuote(ctx context.Context, in *CreateQuoteRequest, opts ...grpc.CallOption) (*CreateQuoteResponse, error) {
	out := new(CreateQuoteResponse)
	err := c.cc.Invoke(ctx, QuotesEntryAPI_CreateQuote_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *quotesEntryAPIClient) CancelQuote(ctx context.Context, in *CancelQuoteRequest, opts ...grpc.CallOption) (*CancelQuoteResponse, error) {
	out := new(CancelQuoteResponse)
	err := c.cc.Invoke(ctx, QuotesEntryAPI_CancelQuote_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *quotesEntryAPIClient) PassQuote(ctx context.Context, in *PassQuoteRequest, opts ...grpc.CallOption) (*PassQuoteResponse, error) {
	out := new(PassQuoteResponse)
	err := c.cc.Invoke(ctx, QuotesEntryAPI_PassQuote_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// QuotesEntryAPIServer is the server API for QuotesEntryAPI service.
// All implementations must embed UnimplementedQuotesEntryAPIServer
// for forward compatibility
type QuotesEntryAPIServer interface {
	// CreateRequestForQuote creates a request for quote on the exchange.
	CreateRequestForQuote(context.Context, *CreateRequestForQuoteRequest) (*CreateRequestForQuoteResponse, error)
	// CreateQuote creates a quote on the exchange.
	CreateQuote(context.Context, *CreateQuoteRequest) (*CreateQuoteResponse, error)
	// CancelQuote cancels a quote on the exchange.
	CancelQuote(context.Context, *CancelQuoteRequest) (*CancelQuoteResponse, error)
	// PassQuote passes on a quote from the exchange.
	PassQuote(context.Context, *PassQuoteRequest) (*PassQuoteResponse, error)
	mustEmbedUnimplementedQuotesEntryAPIServer()
}

// UnimplementedQuotesEntryAPIServer must be embedded to have forward compatible implementations.
type UnimplementedQuotesEntryAPIServer struct {
}

func (UnimplementedQuotesEntryAPIServer) CreateRequestForQuote(context.Context, *CreateRequestForQuoteRequest) (*CreateRequestForQuoteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateRequestForQuote not implemented")
}
func (UnimplementedQuotesEntryAPIServer) CreateQuote(context.Context, *CreateQuoteRequest) (*CreateQuoteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateQuote not implemented")
}
func (UnimplementedQuotesEntryAPIServer) CancelQuote(context.Context, *CancelQuoteRequest) (*CancelQuoteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CancelQuote not implemented")
}
func (UnimplementedQuotesEntryAPIServer) PassQuote(context.Context, *PassQuoteRequest) (*PassQuoteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method PassQuote not implemented")
}
func (UnimplementedQuotesEntryAPIServer) mustEmbedUnimplementedQuotesEntryAPIServer() {}

// UnsafeQuotesEntryAPIServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to QuotesEntryAPIServer will
// result in compilation errors.
type UnsafeQuotesEntryAPIServer interface {
	mustEmbedUnimplementedQuotesEntryAPIServer()
}

func RegisterQuotesEntryAPIServer(s grpc.ServiceRegistrar, srv QuotesEntryAPIServer) {
	s.RegisterService(&QuotesEntryAPI_ServiceDesc, srv)
}

func _QuotesEntryAPI_CreateRequestForQuote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateRequestForQuoteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuotesEntryAPIServer).CreateRequestForQuote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: QuotesEntryAPI_CreateRequestForQuote_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuotesEntryAPIServer).CreateRequestForQuote(ctx, req.(*CreateRequestForQuoteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _QuotesEntryAPI_CreateQuote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateQuoteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuotesEntryAPIServer).CreateQuote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: QuotesEntryAPI_CreateQuote_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuotesEntryAPIServer).CreateQuote(ctx, req.(*CreateQuoteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _QuotesEntryAPI_CancelQuote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CancelQuoteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuotesEntryAPIServer).CancelQuote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: QuotesEntryAPI_CancelQuote_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuotesEntryAPIServer).CancelQuote(ctx, req.(*CancelQuoteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _QuotesEntryAPI_PassQuote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PassQuoteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(QuotesEntryAPIServer).PassQuote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: QuotesEntryAPI_PassQuote_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(QuotesEntryAPIServer).PassQuote(ctx, req.(*PassQuoteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// QuotesEntryAPI_ServiceDesc is the grpc.ServiceDesc for QuotesEntryAPI service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var QuotesEntryAPI_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "connamara.ep3.quotes.v1beta1.QuotesEntryAPI",
	HandlerType: (*QuotesEntryAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateRequestForQuote",
			Handler:    _QuotesEntryAPI_CreateRequestForQuote_Handler,
		},
		{
			MethodName: "CreateQuote",
			Handler:    _QuotesEntryAPI_CreateQuote_Handler,
		},
		{
			MethodName: "CancelQuote",
			Handler:    _QuotesEntryAPI_CancelQuote_Handler,
		},
		{
			MethodName: "PassQuote",
			Handler:    _QuotesEntryAPI_PassQuote_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "connamara/ep3/quotes/v1beta1/quotes_entry_api.proto",
}
